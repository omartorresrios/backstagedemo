//
//  PhotoPostCell.swift
//  example3
//
//  Created by Omar Torres on 20/12/17.
//  Copyright © 2017 Omar Torres. All rights reserved.
//

import UIKit

class PhotoPostCell: UICollectionViewCell {
    
    var delegate: HomePostCellDelegate?
    
    var post: Post? {
        didSet {
            guard let postImageUrl = post?.imageUrl else { return }
            
            likeButton.setImage(post?.hasLiked == true ? #imageLiteral(resourceName: "like_selected").withRenderingMode(.alwaysOriginal) : #imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
            
            photoImageView.image = UIImage(named: "ronaldo")
            
            
            setupAttributedCaption()
        }
    }
    
    fileprivate func setupAttributedCaption() {
        guard let post = self.post else { return }
        
        let attributedText = NSMutableAttributedString(string: "jajaja", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: " \(post.caption)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        attributedText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 4)]))
        
        let timeAgoDisplay = post.creationDate.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        captionLabel.attributedText = attributedText
    }
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "dinho")
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var likeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    lazy var commentButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "comment").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        return button
    }()
    
    @objc func handleComment() {
        print("Trying to show photo comments...")
        guard let post = post else { return }
        
        delegate?.didTapComment()
    }
    
    let sendMessageButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "send2").withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "ribbon").withRenderingMode(.alwaysOriginal), for: .normal)
        return button
    }()
    
    let captionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        
        
        
//        addSubview(usernameLabel)
//        usernameLabel.anchor(top: topAnchor, left: userProfileImageView.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
//        usernameLabel.centerYAnchor.constraint(equalTo: userProfileImageView.centerYAnchor).isActive = true
        
        
        
        let questionLabel = UILabel()
        questionLabel.text = "¿Creen que soy el mejor jugaador de la historia?"
        questionLabel.textAlignment = .center
        questionLabel.backgroundColor = UIColor.white
        questionLabel.textColor = .gray
        questionLabel.numberOfLines = 0
        questionLabel.font = UIFont.boldSystemFont(ofSize: 18)
        questionLabel.clipsToBounds = true
//        addSubview(questionLabel)
//        questionLabel.anchor(top: userProfileImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 40, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: 0, height: 0)
        
        
        
//        let labelOption1 = UILabel()
//        labelOption1.text = "53%"
//        labelOption1.numberOfLines = 0
//        labelOption1.textColor = .gray
//        labelOption1.font = UIFont.boldSystemFont(ofSize: 14)
//        labelOption1.clipsToBounds = true
//        labelOption1.textAlignment = .center
//        labelOption1.backgroundColor = UIColor.white
//        //        labelOption1.addTopBorder(UIColor.gray, height: 0.5)
//        addSubview(labelOption1)
//        labelOption1.anchor(top: questionLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        
        
        
//        let buttonOption1 = UIButton()
//        buttonOption1.setTitle("Jódete", for: .normal)
//        buttonOption1.tintColor = UIColor.white
//        buttonOption1.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
//        buttonOption1.backgroundColor = UIColor.rgb(red: 0, green: 200, blue: 246)
//        buttonOption1.addTopBorder(UIColor.gray, height: 0.5)
//
//        addSubview(buttonOption1)
//        buttonOption1.anchor(top: questionLabel.bottomAnchor, left: labelOption1.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//
//
//
//
//        let labelOption2 = UILabel()
//        labelOption2.text = "47%"
//        labelOption2.numberOfLines = 0
//        labelOption2.textColor = .gray
//        labelOption2.font = UIFont.boldSystemFont(ofSize: 14)
//        labelOption2.clipsToBounds = true
//        labelOption2.textAlignment = .center
//        labelOption2.backgroundColor = UIColor.white
//        //        labelOption2.addTopBorder(UIColor.gray, height: 0.5)
//        //        labelOption2.addBottomBorder(UIColor.gray, height: 0.5)
//        addSubview(labelOption2)
//        labelOption2.anchor(top: labelOption1.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 50, height: 50)
        
        
        
        
//        let buttonOption2 = UIButton()
//        buttonOption2.setTitle("Maldito genio, sigue así!", for: .normal)
//        buttonOption2.tintColor = UIColor.white
//        buttonOption2.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
//        buttonOption2.backgroundColor = UIColor.rgb(red: 0, green: 200, blue: 246)
//        buttonOption2.addTopBorder(UIColor.gray, height: 0.5)
//        buttonOption2.addBottomBorder(UIColor.gray, height: 0.5)
//        addSubview(buttonOption2)
//        buttonOption2.anchor(top: buttonOption1.bottomAnchor, left: labelOption2.rightAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
//        
//        
//        
//        
//        let dateLabel = UILabel()
//        dateLabel.text = "Hace 4 días"
//        dateLabel.numberOfLines = 0
//        dateLabel.font = UIFont.boldSystemFont(ofSize: 10)
//        dateLabel.textColor = .gray
//        dateLabel.clipsToBounds = true
//        dateLabel.textAlignment = .left
//        dateLabel.backgroundColor = UIColor.white
//        addSubview(dateLabel)
//        dateLabel.anchor(top: labelOption2.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
//        
//        
//        
//        
//        let votesLabel = UILabel()
//        votesLabel.text = "1450 votos"
//        votesLabel.numberOfLines = 0
//        votesLabel.font = UIFont.boldSystemFont(ofSize: 10)
//        votesLabel.textColor = .gray
//        votesLabel.clipsToBounds = true
//        votesLabel.textAlignment = .left
//        votesLabel.backgroundColor = UIColor.white
//        addSubview(votesLabel)
//        votesLabel.anchor(top: buttonOption2.bottomAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, width: 0, height: 0)
        
        
        
    }
    
    
    //    fileprivate func setupActionButtons() {
    //        let stackView = UIStackView(arrangedSubviews: [likeButton, commentButton, sendMessageButton])
    //
    //        stackView.distribution = .fillEqually
    //
    //        addSubview(stackView)
    //        stackView.anchor(top: photoImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 4, paddingBottom: 0, paddingRight: 0, width: 120, height: 50)
    //
    //        addSubview(bookmarkButton)
    //        bookmarkButton.anchor(top: photoImageView.bottomAnchor, left: nil, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 40, height: 50)
    //    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


