//
//  Comment.swift
//  InstagramFirebase
//
//  Created by Brian Voong on 5/2/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import Foundation

struct Comment {
    
    
    var text: String?
    var subComments: [Comment]?
    
    init() {
        subComments = [Comment]()
    }
}
