//
//  UserProfileController.swift
//  InstagramFirebase
//
//  Created by Brian Voong on 3/22/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import UIKit
import MediaPlayer

class UserProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UserProfileHeaderDelegate {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        return cell
    }
    
    
    let cellId = "cellId"
    let homePostCellId = "homePostCellId"
    
    var userId: String?
    
    var isGridView = true
    
    func didChangeToGridView() {
        isGridView = true
        collectionView?.reloadData()
    }
    
    func didChangeToListView() {
        isGridView = false
        collectionView?.reloadData()
    }
    
    var mainDictonary = [String: Any]()
    
    let cells = ["ronaldo", "dinho", "thierry"]
    
    override func viewWillAppear(_ animated: Bool) {
       
        super.viewWillAppear(true)
        
        collectionView?.backgroundColor = .white
        collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: homePostCellId)
        
        mainDictonary.updateValue(UIImage(named: "dinho") as Any, forKey: "imageUrl")
        
        let post = Post(dictionary: mainDictonary)
        self.posts.append(post)
        self.collectionView?.reloadData()
        
        fetchUser()
        //fetchOrderedPosts()
    }
    
    var isFinishedPaging = false
    var posts = [Post]()
    
    
    fileprivate func fetchOrderedPosts() {
//        guard let uid = self.user?.uid else { return }
//        let ref = Database.database().reference().child("posts").child(uid)
//        
//        //perhaps later on we'll implement some pagination of data
//        ref.queryOrdered(byChild: "creationDate").observe(.childAdded, with: { (snapshot) in
        
//
//            guard let user = self.user else { return }
//            
        
//
//           self.posts.insert(post, at: 0)
        
//
        
//
//        }) { (err) in
//            print("Failed to fetch ordered posts:", err)
//        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        
        if isGridView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! UserProfilePhotoCell
            
            let image = cells[indexPath.row]
            cell.photoImageView.image = UIImage(named: "\(image)")
            
            
            if indexPath.row == 0 {
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.image = UIImage(named: "poll")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            } else if indexPath.row == 1 {
                
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.image = UIImage(named: "photo")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            } else {
                
                let imageView = UIImageView()
                imageView.image = UIImage(named: "video")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                imageView.contentMode = .scaleAspectFill
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            }
            
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homePostCellId, for: indexPath) as! HomePostCell
            
            let image = cells[indexPath.row]
            cell.photoImageView.image = UIImage(named: "\(image)")
            
            
            if indexPath.row == 0 {
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.image = UIImage(named: "poll")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            } else if indexPath.row == 1 {
                
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFill
                imageView.image = UIImage(named: "photo")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            } else {
                
                let imageView = UIImageView()
                imageView.image = UIImage(named: "video")?.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = .white
                imageView.contentMode = .scaleAspectFill
                cell.photoImageView.addSubview(imageView)
                imageView.anchor(top: cell.photoImageView.topAnchor, left: nil, bottom: nil, right: cell.photoImageView.rightAnchor, paddingTop: 4, paddingLeft: 0, paddingBottom: 0, paddingRight: 4, width: 15, height: 15)
                
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isGridView {
            let width = (view.frame.width - 2) / 3
            return CGSize(width: width, height: width)
        } else {
            
            var height: CGFloat = 40 + 8 + 8 //username userprofileimageview
            height += view.frame.width
            height += 50
            height += 60
            
            return CGSize(width: view.frame.width, height: height)
        }
    }
    
    let photoLauncher = PhotoLauncher()
    let videoLauncher = VideoLauncher()
    let pollLauncher = PollLauncher()
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            self.view.addSubview(self.pollLauncher)
            self.pollLauncher.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
            
            tabBarController?.tabBar.isHidden = true
            navigationController?.navigationBar.isHidden = true
            
            let photoTap  = UITapGestureRecognizer(target: self, action: #selector(dismissPollLauncher))
            pollLauncher.addGestureRecognizer(photoTap)
            
        } else if indexPath.row == 1 {
            
            self.view.addSubview(self.photoLauncher)
            self.photoLauncher.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
                
            tabBarController?.tabBar.isHidden = true
            navigationController?.navigationBar.isHidden = true
            
            let photoTap  = UITapGestureRecognizer(target: self, action: #selector(dismissPhotoLauncher))
            photoLauncher.addGestureRecognizer(photoTap)
            
        } else {
            
            self.view.addSubview(self.videoLauncher)
            self.videoLauncher.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
            
            let videoString:String? = Bundle.main.path(forResource: "video", ofType: ".mp4")
            
            if let url = videoString {
                let videoURL = NSURL(fileURLWithPath: url)
                videoLauncher.player = AVPlayer(url: videoURL as URL)
                let playerLayer = AVPlayerLayer(player: videoLauncher.player)
                playerLayer.frame = view.bounds
                videoLauncher.view.layer.addSublayer(playerLayer)
                videoLauncher.player.play()
            } else {
                print("THERES A PROBLEM")
            }
            
            tabBarController?.tabBar.isHidden = true
            navigationController?.navigationBar.isHidden = true
            
            let photoTap  = UITapGestureRecognizer(target: self, action: #selector(dismissVideoLauncher))
            videoLauncher.addGestureRecognizer(photoTap)
            
        }
    }
    
    @objc func dismissPollLauncher() {
        pollLauncher.removeFromSuperview()
        videoLauncher.player.pause()
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func dismissPhotoLauncher() {
        photoLauncher.removeFromSuperview()
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func dismissVideoLauncher() {
        videoLauncher.removeFromSuperview()
        tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.isHidden = false
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! UserProfileHeader
        
       
        header.delegate = self
        
        
        
//        header.profileImageView.image = UIImage(named: "\(image)")
        header.usernameLabel.text = "Ronaldo"
        
        //not correct
        //header.addSubview(UIImageView())
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
    var user: User?
    fileprivate func fetchUser() {
        
//        let uid = userId ?? (Auth.auth().currentUser?.uid ?? "")
//        
//        //guard let uid = Auth.auth().currentUser?.uid else { return }
//        
//        Database.fetchUserWithUID(uid: uid) { (user) in
//            self.user = user
//            self.navigationItem.title = self.user?.username
//            
//            self.collectionView?.reloadData()
//            
//            self.paginatePosts()
//        }
    }
}





