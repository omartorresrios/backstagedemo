//
//  UserMentionsController.swift
//  example3
//
//  Created by Omar Torres on 3/01/18.
//  Copyright © 2018 Omar Torres. All rights reserved.
//

import UIKit

class UserMentionsController: UIViewController {
    
    var lastTextViewHeight:CGFloat = 0.0
    
    var messages:[String] = ["regular text, nothing to see here", "#ribl <- click on it to see an alert", "@riblapp mention tags work too", "regular urls are clickable http://ribl.co", "add your own text below"]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textviewHeight: NSLayoutConstraint!
    @IBOutlet weak var toolbarBottom: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeholderText: UILabel!
    
    @IBAction func sendButton(sender: AnyObject) {
        textView.endEditing(true)
        messages.append(textView.text)
        // clear the text
        textView.text = ""
        // and manually trigger the delegate method
        self.textViewDidChange(textView)
        tableView.reloadData()
        print("user mentions loaded!")
        view.backgroundColor = .yellow
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // listen for the keyboard going up and down
        keyboardHeightRegisterNotifications()
    }
}

// MARK: - Keyboard helper methods

// all this just to move the keyboard up and down.
extension UIViewController {
    
    /// register keyboard notifications to shift the scrollview content insets
    func keyboardHeightRegisterNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    /// just pass true or false if you're shifting the keyboard up or down
    @objc func keyboardWillShow(notification: NSNotification) {
        adjustInsetForKeyboardShow(show: true, notification: notification)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        adjustInsetForKeyboardShow(show: false, notification: notification)
    }
    
    /// consolidate the keyboard movement logic into one method, and just pass a boolean for up or down.
    func adjustInsetForKeyboardShow(show: Bool, notification: NSNotification) {
        // some implementations out there use -1 and 1 to move it up or down.
        // debugging is a little easier if you use -1 and 0 instead.
        let userMentionsController = UserMentionsController()
        userMentionsController.toolbarBottom.constant = getKeyboardHeight(notification: notification) * (show ? 1 : 0)
        // normally, the constraint change is updated immediately.
        // by simply added UIView.animateWithDuration along with a layoutIfNeeded(),
        // the constraint change will happen in the animation.
        // the animation settings below sort of match the keyboard animation
        UIView.animate(withDuration: 0.5,
                                   delay: 0.0,
                                   options: .curveEaseOut,
                                   animations: {
                                    // animate the constraint change
                                    self.view.layoutIfNeeded()
        },
                                   completion: nil
        )
    }
    
    func getKeyboardHeight(notification: NSNotification) -> CGFloat{
        // == userInfo || {}
        let userInfo = notification.userInfo ?? [:]
        // CGRect wrapped in a NSValue
        // Make sure you use UIKeyboardFrameEndUserInfoKey, NOT UIKeyboardFrameBeginUserInfoKey
        // "End" is good.  "Begin" is bad.
        // To test, switch keyboards and make sure the heights are correct.
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        return keyboardFrame.height
    }
    
}

// MARK: - UITableViewDataSource methods

extension UIViewController : UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let userMentionsController = UserMentionsController()
        return userMentionsController.messages.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userMentionsController = UserMentionsController()
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TextCell
        cell.setCellText(text: userMentionsController.messages[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate methods

extension UIViewController : UITableViewDelegate {
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // set the height of the row based on the text height.
        // TODO: there's probably a better way to do this.
        let userMentionsController = UserMentionsController()
        
        // type cast to NSString to get additional methods
        let myString: NSString = userMentionsController.messages[indexPath.row] as NSString
        // label height depends on font
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18.0)]
        let stringWidth:CGFloat = UIScreen.main.bounds.width - 20
        // hard-coding a really big number that exceeds the screen height
        let infiniteHeight:CGFloat = 1600
        let temporarySize = CGSize(width: stringWidth, height: infiniteHeight)
        let rect:CGRect = myString.boundingRect(with: temporarySize, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return rect.height + 30
    }
    
}

// MARK: - UITextViewDelegate methods

extension UIViewController : UITextViewDelegate {
    
    // increase the height of the textview as the user types
    public func textViewDidChange(_ textView: UITextView){
        // hide placeholder text
        let userMentionsController = UserMentionsController()
        userMentionsController.placeholderText.isHidden = !textView.text.isEmpty
        // create a hypothetical tall box that contains the text.
        // then shrink down the height based on the content.
        let newSize:CGSize = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: 1600.0))
        // remember that new height
        let newHeight = newSize.height
        // change the height constraint only if it's different.
        // otherwise, it get set on every single character the user types.
        if userMentionsController.lastTextViewHeight != newHeight {
            userMentionsController.lastTextViewHeight = newHeight
            // the 7.0 is to account for the top of the text getting scrolled up slightly
            // to account for a potential new line
            userMentionsController.textviewHeight.constant = newSize.height + 7.0
        }
    }
    
    func showHashTagAlert(tagType:String, payload:String){
        let alertView = UIAlertView()
        alertView.title = "\(tagType) tag detected"
        // get a handle on the payload
        alertView.message = "\(payload)"
        alertView.addButton(withTitle: "Ok")
        alertView.show()
    }
    
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        // check for our fake URL scheme hash:helloWorld
        switch URL.scheme {
        case "hash"? :
            showHashTagAlert(tagType: "hash", payload: ((URL as NSURL).resourceSpecifier?.removingPercentEncoding!)!)
        case "mention"? :
            showHashTagAlert(tagType: "mention", payload: ((URL as NSURL).resourceSpecifier?.removingPercentEncoding!)!)
        default:
            print("just a regular url")
        }
        
        return true
    }
    
}
