//
//  textCell.swift
//  example3
//
//  Created by Omar Torres on 4/01/18.
//  Copyright © 2018 Omar Torres. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    
    func setCellText(text:String){
        textView.text = text
        textView.resolveHashTags()
    }

}
