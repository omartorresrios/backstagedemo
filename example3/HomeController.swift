//
//  HomeController.swift
//  InstagramFirebase
//
//  Created by Brian Voong on 4/6/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HomePostCellDelegate {
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: SharePhotoController.updateFeedNotificationName, object: nil)
        
        collectionView?.backgroundColor = .white
        
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: cellId)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        setupNavigationItems()
        
        fetchPostsWithUser()
    }
    
    func didLike() {
        
    }
    
    func didTapComment() {
        print("Message coming from HomeController")
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(commentsController, animated: true)
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        print("Handling refresh..")
        posts.removeAll()
        fetchAllPosts()
    }
    
    fileprivate func fetchAllPosts() {
//        fetchPosts()
    }
    
    //iOS9
    //let refreshControl = UIRefreshControl()
    
    var posts = [[String: Any]]()
//    fileprivate func fetchPosts() {
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//
//        Database.fetchUserWithUID(uid: uid) { (user) in
//            self.fetchPostsWithUser(user: user)
//        }
//    }
    
    var postDictionary = [String: Any]()
    
    func fetchPostsWithUser() {
        
        postDictionary.updateValue(UIImage(named: "dinho"), forKey: "imageUrl")
        postDictionary.updateValue("the best footbal player ever!", forKey: "caption")
        
        posts.append(postDictionary)
        collectionView?.reloadData()
        
    }
    
    func setupNavigationItems() {
                
        navigationItem.title = "App demo"
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 40 + 8 + 8 //username userprofileimageview
        height += view.frame.width
        height += 50
        height += 60
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomePostCell
        
        cell.delegate = self
        
        let post = posts[indexPath.item]
        cell.photoImageView.image = post["imageUrl"] as? UIImage
        
        return cell
    }
    
}
